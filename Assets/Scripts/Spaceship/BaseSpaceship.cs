using UnityEngine;

namespace Spaceship
{
    public abstract class BaseSpaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected Transform gunPosition;


        protected float Hp { get; set; }
        protected float FullHp { get; set; }
        public float Speed { get; private set; }
        private Bullet Bullet { get; set; }

        protected void Init(int hp, float speed, Bullet bullet)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
        }

        public abstract void Fire();
    }
}